package oshelper

import (
	"runtime"
)

func PathSeparator () string {
	if runtime.GOOS == "windows" {
		return "\\"
	} else {
		return "/"
	}
}