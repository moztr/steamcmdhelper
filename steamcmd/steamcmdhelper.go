package steamcmd

import (
	"os/exec"
	"fmt"
	"os"
	"bufio"
	"runtime"
	"gitlab.com/moztr/steamcmdhelper/oshelper"
	"gitlab.com/moztr/steamcmdhelper/sdtd"
)

type SteamApplication struct {
	Steamcmd   Steamcmd
	Installdir string
	Appid      string
	Usebeta    bool
	Betabranch string
	Config sdtd.Config
}

func buildInstallArgs(sa SteamApplication) [] string {
	pwd, err := os.Getwd()
	if err != nil {
		fmt.Println("Error: ")
		fmt.Println(err)
		panic("Ooops, could not figure out my current working directory")
	}

	if sa.Usebeta {
		return []string{"+login",
			"anonymous",
			"+force_install_dir",
			pwd + oshelper.PathSeparator() + sa.Installdir,
			"+app_update",
			sa.Appid,
			"-beta",
			sa.Betabranch,
			"validate",
			"+quit"}
	} else {
		return []string{"+login",
			"anonymous",
			"+force_install_dir",
			pwd + oshelper.PathSeparator() + sa.Installdir,
			"+app_update",
			sa.Appid,
			"validate",
			"+quit"}
	}
}

func (sa SteamApplication) InstallApplication() {

	cmdName := sa.Steamcmd.Path
	if runtime.GOOS == "windows" {
		cmdName += "\\steamcmd.exe"
	} else {
		cmdName += "/steamcmd.sh"
	}
	cmdArgs := buildInstallArgs(sa)

	fmt.Fprintln(os.Stdout, "Going to execute: ", cmdName, cmdArgs)

	cmd := exec.Command(cmdName, cmdArgs...)
	cmdReader, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error creating StdoutPipe for Cmd", err)
		os.Exit(1)
	}

	scanner := bufio.NewScanner(cmdReader)
	go func() {
		for scanner.Scan() {
			fmt.Printf("steamcmd.sh | %s\n", scanner.Text())
		}
	}()

	err = cmd.Start()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error starting Cmd", err)
		os.Exit(1)
	}

	err = cmd.Wait()
	if err != nil {
		if serr, ok := err.(*exec.ExitError); ok {
			println(serr)
			processSuccess := serr.ProcessState.Success()
			if !processSuccess {
				fmt.Fprintln(os.Stderr, "Error with process failure. Did you forget to install 32bit libs? ( sudo apt-get install lib32gcc1 )", serr)
				os.Exit(1)
			} else {
				fmt.Fprintln(os.Stderr, "Error ignored because assumed not fatal", serr)
			}
		}
	}

	fmt.Println("Finished installation/verification")
}


func (sa SteamApplication) StartApplication() error {
	cmdName := sa.Installdir
	if runtime.GOOS == "windows" {
		cmdName += "\\7DaysToDieServer.exe"
	} else {
		cmdName += "/7DaysToDieServer.x86_64"
	}
	fmt.Println("cmdName is: '" + cmdName + "'")

	cmdArgs := sa.buildStartArgs()
	cmd := exec.Command(cmdName, cmdArgs...)
	cmdReader, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error creating StdoutPipe for Cmd", err)
		os.Exit(1)
	}

	scanner := bufio.NewScanner(cmdReader)
	go func() {
		for scanner.Scan() {
			fmt.Printf("sdtd | %s\n", scanner.Text())
		}
	}()

	err = cmd.Start()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error starting Cmd", err)
		os.Exit(1)
	}

	go sdtd.SdtdTelnetConnect(sa.Config)

	fmt.Println("sdtd | Waiting for command to end")
	err = cmd.Wait()
	if err != nil {
		fmt.Fprintln(os.Stderr, "Error waiting for Cmd", err)
		os.Exit(1)
	}
	fmt.Println("Joined")
	return nil
}

func (sa SteamApplication) buildStartArgs() [] string {
	// No dedicated - this would put the server in background which we do NOT want
	//		"-dedicated",
	return [] string{
		"-batchmode",
		"-nographics",
		"-dedicated",
		"-configfile=" + sa.Installdir + oshelper.PathSeparator() + "server.xml",
		"-logfile",
		"currentlog.txt",
	}
}
