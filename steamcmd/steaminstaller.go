package steamcmd

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
	"errors"
	"archive/zip"
	"gitlab.com/moztr/steamcmdhelper/oshelper"
)

type Steamcmd struct {
	Path string
}

/**
Ensure steamcmd is installed and is up to date in the directory that was set
*/
func (steamcmd Steamcmd) EnsureInstalled(forceReinstall bool) error {

	steamcmdUrlLinux := "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz"
	steamcmdUrlWindows := "https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip"

	// First check if steamcmd.sh is present
	if forceReinstall {
		fmt.Println("ForceReinstall detected, will delete directory first...")
		if _, err := os.Stat(steamcmd.Path + oshelper.PathSeparator()); os.IsNotExist(err) {
			fmt.Println("Could not find directry, skip delete")
		} else {
			err := os.RemoveAll(steamcmd.Path + oshelper.PathSeparator())
			if err != nil {
				mypanic(err, "Could not delete path: '"+steamcmd.Path+oshelper.PathSeparator()+"'")
			}
		}
	}

	if _, err := os.Stat(steamcmd.Path); os.IsNotExist(err) {
		fmt.Println("Could not find directry, create it")

		if err := os.MkdirAll(steamcmd.Path, 0777); err != nil {
			mypanic(err, "Cannot create '"+steamcmd.Path+"'")
		}
	}

	if runtime.GOOS == "windows" {
		steamcmdPath := steamcmd.Path + "\\steamcmd.exe"
		if _, err := os.Stat(steamcmdPath); os.IsNotExist(err) {
			fmt.Println("Could not find steamcmd in '" + steamcmdPath + "', attempting to download ...")
			out, err := os.Create(steamcmdPath + ".zip")
			defer out.Close()
			if err != nil {
				mypanic(err, "Could not create file in '"+steamcmdPath+"'")
			}
			resp, err := http.Get(steamcmdUrlWindows)
			if err != nil {
				mypanic(err, "Could not download file")
			}
			defer resp.Body.Close()
			n, err := io.Copy(out, resp.Body)
			if err != nil {
				mypanic(err, "Could not write file")
			}
			fmt.Println("Wrote bytes", n)

			err = unzip(steamcmdPath + ".zip", steamcmd.Path)
			if err != nil {
				mypanic(err, "failed to unzip steamcmd")
			}
			fmt.Println("Done!")
		}
	} else if runtime.GOOS == "linux" {
		steamcmdPath := steamcmd.Path + "/steamcmd.sh"
		if _, err := os.Stat(steamcmdPath); os.IsNotExist(err) {
			fmt.Println("Could not find steamcmd in '" + steamcmdPath + "', attempting to download ...")
			out, err := os.Create(steamcmdPath + ".tar.gz")
			defer out.Close()
			if err != nil {
				mypanic(err, "Could not create file in '"+steamcmdPath+"'")
			}
			resp, err := http.Get(steamcmdUrlLinux)
			if err != nil {
				mypanic(err, "Could not download file")
			}
			defer resp.Body.Close()
			n, err := io.Copy(out, resp.Body)
			if err != nil {
				mypanic(err, "Could not write file")
			}
			fmt.Println("Wrote bytes", n)

			fmt.Println("Extracting ...")
			err = ungzip(steamcmdPath+".tar.gz", steamcmdPath+".tar")
			if err != nil {
				mypanic(err, "Could not ungzip")
			}
			err = untar(steamcmdPath+".tar", steamcmd.Path)
			if err != nil {
				mypanic(err, "Could not untar")
			}
			fmt.Println("Done!")

		} else {
			fmt.Println("Found steamcmd in '" + steamcmdPath + "', skipping download.")
		}
	} else {
		return errors.New("OS not supported: " + runtime.GOOS)
	}


	return nil
}

func mypanic(err error, msg string) {
	fmt.Println("Error occured, panic and run!")
	fmt.Println(err)
	panic(msg)
}

func ungzip(source, target string) error {
	reader, err := os.Open(source)
	if err != nil {
		return err
	}
	defer reader.Close()

	archive, err := gzip.NewReader(reader)
	if err != nil {
		return err
	}
	defer archive.Close()

	target = filepath.Join(target, archive.Name)
	writer, err := os.Create(target)
	if err != nil {
		return err
	}
	defer writer.Close()

	_, err = io.Copy(writer, archive)
	return err
}

func untar(tarball, target string) error {
	reader, err := os.Open(tarball)
	if err != nil {
		return err
	}
	defer reader.Close()
	tarReader := tar.NewReader(reader)

	for {
		header, err := tarReader.Next()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		path := filepath.Join(target, header.Name)
		info := header.FileInfo()
		if info.IsDir() {
			if err = os.MkdirAll(path, info.Mode()); err != nil {
				return err
			}
			continue
		}

		file, err := os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, info.Mode())
		if err != nil {
			return err
		}
		defer file.Close()
		_, err = io.Copy(file, tarReader)
		if err != nil {
			return err
		}
	}
	return nil
}

func unzip(src, dest string) error {
	r, err := zip.OpenReader(src)
	if err != nil {
		return err
	}
	defer func() {
		if err := r.Close(); err != nil {
			panic(err)
		}
	}()

	os.MkdirAll(dest, 0755)

	// Closure to address file descriptors issue with all the deferred .Close() methods
	extractAndWriteFile := func(f *zip.File) error {
		rc, err := f.Open()
		if err != nil {
			return err
		}
		defer func() {
			if err := rc.Close(); err != nil {
				panic(err)
			}
		}()

		path := filepath.Join(dest, f.Name)

		if f.FileInfo().IsDir() {
			os.MkdirAll(path, f.Mode())
		} else {
			os.MkdirAll(filepath.Dir(path), f.Mode())
			f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
			if err != nil {
				return err
			}
			defer func() {
				if err := f.Close(); err != nil {
					panic(err)
				}
			}()

			_, err = io.Copy(f, rc)
			if err != nil {
				return err
			}
		}
		return nil
	}

	for _, f := range r.File {
		err := extractAndWriteFile(f)
		if err != nil {
			return err
		}
	}

	return nil
}
