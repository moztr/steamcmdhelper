package sdtd

func New () Config {
	return Config{
		ServerPort : "26900",
		ServerIsPublic : "false",
		ServerName : "My Game Host",
		ServerPassword : "",
		ServerMaxPlayerCount : "8",
		ServerDescription : "A 7 Days to Die server",
		ServerWebsiteURL : "",
		GameWorld : "Navezgane", // or "Random Gen"
		GameName : "My Game",
		GameDifficulty : "2",
		GameMode : "GameModeSurvivalMP",
		ZombiesRun : "0",
		BuildCreate : "false",
		DayNightLength : "50",
		DayLightLength : "18",
		PlayerKillingMode : "3",
		PersistentPlayerProfiles : "false",
		PlayerSafeZoneLevel : "5",
		PlayerSafeZoneHours : "5",
		BloodMoonEnemyCount : "8",
		ControlPanelEnabled : "false",
		ControlPanelPort : "48080",
		ControlPanelPassword : "CHANGEME",
		TelnetEnabled : "true",
		TelnetPort : "48081",
		TelnetPassword : "test123",
		AdminFileName : "serveradmin.xml",
		DropOnDeath : "0",
		DropOnQuit : "0",
		EnemySenseMemory : "60",
		EnemySpawnMode : "true", // on or off
		EnemyDifficulty : "0",
		BlockDurabilityModifier : "100",
		LootAbundance : "100",
		LootRespawnDays : "30",
		LandClaimSize : "7",
		LandClaimDeadZone : "30",
		LandClaimExpiryTime : "3",
		LandClaimDecayMode : "0",
		LandClaimOnlineDurabilityModifier : "4",
		LandClaimOfflineDurabilityModifier :  "4",
		AirDropFrequency : "72",
		AirDropMarker : "false",
		MaxSpawnedZombies : "60",
		MaxSpawnedAnimals : "50",
		EACEnabled : "true",
		SaveGameFolder : "savedir",

	}  // enforce the default value here
}

type Config struct {
	ServerPort string
	ServerIsPublic string
	ServerName string
	ServerPassword string
	ServerMaxPlayerCount string
	ServerDescription string
	ServerWebsiteURL string
	GameWorld string
	GameName string
	GameDifficulty string
	GameMode string
	ZombiesRun string
	BuildCreate string
	DayNightLength string
	DayLightLength string
	PlayerKillingMode string
	PersistentPlayerProfiles string
	PlayerSafeZoneLevel string
	PlayerSafeZoneHours string
	BloodMoonEnemyCount string
	ControlPanelEnabled string
	ControlPanelPort string
	ControlPanelPassword string
	TelnetEnabled string
	TelnetPort string
	TelnetPassword string
	AdminFileName string
	DropOnDeath string
	DropOnQuit string
	EnemySenseMemory string
	EnemySpawnMode string
	EnemyDifficulty string
	BlockDurabilityModifier string
	LootAbundance string
	LootRespawnDays string
	LandClaimSize string
	LandClaimDeadZone string
	LandClaimExpiryTime string
	LandClaimDecayMode string
	LandClaimOnlineDurabilityModifier string
	LandClaimOfflineDurabilityModifier string
	AirDropFrequency string
	AirDropMarker string
	MaxSpawnedZombies string
	MaxSpawnedAnimals string
	EACEnabled string
	SaveGameFolder string
}
