package sdtd

import (
	"github.com/ziutek/telnet"
	"log"
	"time"
	"regexp"
)

const timeout = 10 * time.Second

func checkErr(err error) {
	if err != nil {
		log.Fatalln("Error:", err)
	}
}

type errorString struct {
	s string
}

func (e *errorString) Error() string {
	return e.s
}

func expect(t *telnet.Conn, d ...string) error {
	timeout := make(chan bool, 1)
	go func() {
		time.Sleep(1 * time.Second)
		timeout <- true
	}()

	ch := make(chan bool, 1)
	go func() {
		checkErr(t.SkipUntil(d...))
		ch <- true
	}()

	select {
	case <-ch:
		// a read from ch has occurred
		log.Println("telnet | found ", d)
		return nil
	case <-timeout:
		return &errorString{"Timeout occurred"}
	}
}

func sendln(t *telnet.Conn, s string) {
	checkErr(t.SetWriteDeadline(time.Now().Add(timeout)))
	buf := make([]byte, len(s)+1)
	copy(buf, s)
	buf[len(s)] = '\n'
	_, err := t.Write(buf)
	checkErr(err)
}

func SdtdTelnetConnect(config Config) {

	var t *telnet.Conn
	var err error
	i := 0
	for {
		println("telnet | Try no ", i)
		t, err = telnet.Dial("tcp", "localhost:"+config.TelnetPort)
		i++
		if err == nil {
			println("telnet | Connection established")
			break
		}
		if i > 10 {
			checkErr(err)
			println("telnet | Aborting, more than 10 tries")
			return
		}
		println("telnet | Connection refused, wait 5 seconds...")
		time.Sleep(5000 * time.Millisecond)
	}
	t.SetUnixWriteMode(true)

	if expect(t, "Please enter password:") != nil {
		log.Fatalln("telnet | Expected input not found")
		t.Close()
		return
	}
	sendln(t, config.TelnetPassword)
	if expect(t, "Logon successful.") != nil {
		log.Fatalln("telnet | Expected input not found")
		t.Close()
		return
	}
	if expect(t, "Press 'help' to get a list of all commands. Press 'exit' to end session.") != nil {
		log.Fatalln("telnet | Expected input not found")
		t.Close()
		return
	}

	println("Sleep 5 seconds")
	time.Sleep(5000 * time.Millisecond)
	sendln(t, "shutdown")

	//"2017-12-27T20:47:18 19.878 INF Cleanup"
	lastServerline, err := regexp.Compile(".*INF Cleanup.*")
	if err != nil {
		panic(err)
	}

	defer t.Close()

	for {
		data, err := t.ReadString('\n')
		if err != nil {
			println("Error while reading, aborting")
			return
		}
		println(data)
		if lastServerline.MatchString(data) {
			println("Last line detected, exiting")
			return
		}
	}
}
