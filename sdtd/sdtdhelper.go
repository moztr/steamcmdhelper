package sdtd

import (
	"os"
	"text/template"
)

func WriteTemplate(filename string) error {
	serverxml := New()

	tmpl, err := template.New("7dtd_server.xml").Parse(SdtdServerXmlTemplate())
	if err != nil { return err }

	outfile,err := os.Create(filename)
	if err != nil { return err }

	err = tmpl.Execute(outfile, serverxml)
	if err != nil { return err }

	outfile.Close()

	return nil
}