package main

import (
	"gitlab.com/moztr/steamcmdhelper/steamcmd"
	"os"
	"fmt"
	"gitlab.com/moztr/steamcmdhelper/oshelper"
	"gitlab.com/moztr/steamcmdhelper/sdtd"
    //"gopkg.in/resty.v1"
	//"bytes"
)

type Server struct {
	uuid string
	name string
}

func main() {
	pwd, err := os.Getwd();
	if err != nil {
		fmt.Println("Error: ")
		fmt.Println(err)
		panic("Ooops");
	}

/*	resp, err := resty.R().SetResult(&[]Server{}).Get("http://192.168.0.158:8087/servermanager/server")
	s := resp.Result()

	fmt.Println("resp is: ", s)*/
	fmt.Println("Current working directory is: " + pwd)


	for {
		fmt.Println("The big for loop starts")

		// The big for loop
		steamcmd2 := steamcmd.Steamcmd{Path: "steamcmd_dir"}
		// Not using this feature yet, so don't check if it's installed
		err = steamcmd2.EnsureInstalled(false)
		if err != nil {
			fmt.Println("Could not install steamcmd, error is " + err.Error())
			os.Exit(-1)
		}

		sdtdSteam := steamcmd.SteamApplication{
			Steamcmd: steamcmd2,
			Installdir:"sdtd",
			Appid: "294420",
			Usebeta: false,
			Betabranch: "latest_experimental",
			Config: sdtd.New(),
			}
		// assume the application was manually installed
		sdtdSteam.InstallApplication()

		// assume the configfile was manually written

		templatePath := sdtdSteam.Installdir + oshelper.PathSeparator() + "server.xml"
		err = sdtd.WriteTemplate(templatePath)
		fmt.Fprintln(os.Stdout, "Server configuration written to: ", templatePath)
		if err != nil { panic("Error writing server.xml template to file: '"+templatePath+"'" )}


		err = sdtdSteam.StartApplication()
		if err != nil {
			fmt.Println("Error while starting application")
			fmt.Println(err)
			os.Exit(-2)
		}

	}

}
